FROM node:12-alpine

WORKDIR /src

COPY . /src

RUN npm install

RUN npm install -g nodemon

CMD ["npm", "start"]
