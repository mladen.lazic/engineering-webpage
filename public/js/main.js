const name = user.nickname;
const firstName = (name.charAt(0).toUpperCase() + name.slice(1)).slice(0, -2);
const lastName = name.toUpperCase().split('')[name.toUpperCase().split('').length - 1];
const email = user.email;

const keyTools = [
    { "name" : "Gitea",
      "path" : "https://gitea.scopicsoftware.com"},
    { "name" : "JIRA",
      "path" : "https://scopicsoftware.atlassian.net"},
    { "name" : "DSSP",
      "path" : "https://dssp.scopicsoftware.com"},
    { "name" : "Passbolt",
      "path" : "https://passbolt.scopicsoftware.com"},
    { "name" : "Rancher",
      "path" : "https://rancher.scopicsoftware.com"},
    ];

const keyMembers = [
    { "name" : "Mladen Lazic",
      "position" : "VP of Engineering & Operations"},
    { "name" : "Josip Radic",
      "position" : "Deputy Director of Engineering"},
    { "name" : "Mila Milosavljevic",
      "position" : "IT Operations Manager"},
    ];

const reviewRequests = [
    { "title" : "Request Estimates",
      "name" : "Review",
      "icon" : "far fa-clock",
      "frameUrl" : "https://form.jotformeu.com/82416815676364"},
    { "title" : "Request Architecture",
      "name" : "Document Review",
      "icon" : "far fa-file-alt",
      "frameUrl" : "https://form.jotformeu.com/82456552676366"},
    { "title" : "Request",
      "name" : "IT Support",
      "icon" : "fas fa-user-friends",
      "frameUrl" : "https://form.jotformeu.com/201984293564363"}
]

function generateDynamicUrl() {
    reviewRequests[0].frameUrl += "?reviewRequested[first]=" + firstName +
        "&reviewRequested[last]=" + lastName;
    reviewRequests[1].frameUrl += "?reviewRequested[first]=" + firstName +
        "&reviewRequested[last]=" + lastName;
    reviewRequests[2].frameUrl += "?email6=" + encodeURIComponent(email) +
        "&reporter[first]=" + firstName + "&reporter[last]=" + lastName;
}

generateDynamicUrl();

function getName() {
    return firstName + " " + lastName;
}

function setFrameUrl(url) {
    let frame = document.getElementById('frm');
    frame.src = url;
}

document.addEventListener('DOMContentLoaded', function() {
    const listTools = document.getElementById('listKeyTools');
    const listMembers = document.getElementById('listKeyMembers');
    const listRequests = document.getElementById('listRequests');
    keyTools.map((element) => {
        const button = document.createElement('button');
        button.type = 'button';
        button.innerHTML = element.name;
        button.className = 'btn btn-outline-primary m-1';
        button.onclick = function () {
            window.open(element.path);
        };
        listTools.appendChild(button);
    });
    keyMembers.map((member) => {
        listMembers.innerHTML += `<li class="item"><i class="far fa-user"></i>${member.name}
            <small class="m-0 p-0">${member.position}</small><hr class="mt-1 mb-0"/></li>`;
    });
    reviewRequests.map((request) => {
        listRequests.innerHTML += `<li class="item" onclick='setFrameUrl("${request.frameUrl}")'>
            <i class="${request.icon}"></i>${request.title}<br/>${request.name}</li>`;
    });
    document.getElementById('navbardrop').insertAdjacentHTML("afterbegin", getName());
}, false);

