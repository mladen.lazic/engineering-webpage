const router = require('express').Router();
const jsStringify = require('js-stringify');

router.get('/', (req, res) => {
    if (req.oidc.isAuthenticated()) {
        const user = res.locals.user;
        res.render('index', {jsStringify, user});
    } else {
        res.redirect('/login')
    }
});

module.exports = router;
