const express = require('express');
const http = require('http');
const router = require('./routes/index');
const auth = require('express-openid-connect');
const path = require('path');
require('dotenv').config();
const favicon = require('serve-favicon');

const app = express();

const config = {
    authRequired: false,
    auth0Logout: true,
};
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(auth.auth(config));

app.use(function (req, res, next) {
    res.locals.user = req.oidc.user;
    next();
});

app.use('/', router);
app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));
app.use(express.static(path.join(__dirname, 'public')));

http.createServer(app)
    .listen(process.env.PORT, () => {
        console.log(`Listening on ${process.env.BASE_URL}`);
    });
